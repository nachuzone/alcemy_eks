# Configure the AWS Provider
provider "aws" {
  region = "eu-central-1"
}
resource "aws_ecr_repository" "nachiketuecr" {
  name                 = "ecr"
  image_tag_mutability = "MUTABLE"

  image_scanning_configuration {
    scan_on_push = true
  }
}